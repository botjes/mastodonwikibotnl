## Hi!
This is a script that finds yesterday's most viewed Dutch language Wikipedia page and toots about it on [mastodon.social@WikiVanGister](https://mastodon.social/@WikiVanGister). 

Feel free to make your own version in another language using my code. If you do, please credit me and let me know, so I can follow your bot!


